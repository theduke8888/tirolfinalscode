Category.create!(name: 'Accounting / Finance')
Category.create!(name: 'Admin / Office / Clerical')
Category.create!(name: 'Agriculture / Veterinary')
Category.create!(name: 'Airline / Airport')
Category.create!(name: 'Arts / Media / Design')
Category.create!(name: 'Call Center / BPO')
Category.create!(name: 'Domestic / Caretaker')
Category.create!(name: 'Education / Schools')
Category.create!(name: 'Food / Restaurant')
Category.create!(name: 'Foreign Language')
Category.create!(name: 'Government  / Non-profit')
Category.create!(name: 'Health / Medical / Science')
Category.create!(name: 'Hotel / Spa / Salon')
Category.create!(name: 'Hr / Recruitment / Training')
Category.create!(name: 'Legal / Documentation')
Category.create!(name: 'Logistics / Warehousing')
Category.create!(name: 'Maritime / Seabased')
Category.create!(name: 'Production / Manufacturing')
Category.create!(name: 'Purchasing / Buying')
Category.create!(name: 'Sales / Marketing /  Retail')
Category.create!(name: 'Skilled Work / Technical')
Category.create!(name: 'Sports / Atheletic')
Category.create!(name: 'Internship')
Category.create!(name: 'IT / Computer')
Category.create!(name: 'Others')

10.times do |n|
	name				= Faker::Name.name
	company				= Faker::Company.name
	address				= Faker::Address.street_address + " " + Faker::Address.street_name + " " + Faker::Address.city
	overview			= Faker::Lorem.sentences(25)
	working_hours		= "Monday - Friday"
	benefits			= Faker::Lorem.sentences(3)
	email				= "user-#{n+1}@gmail.com"
	password			= "123456"
	sign_in_count		= 0
	confirmation_token	= Faker::Bitcoin.address
	confirmed_at		= Time.zone.now
	failed_attempts		= 0
	
	User.create!(
			name: 				name,
			company: 			company,
			address:  			address,
			overview: 			overview,
			working_hours: 		working_hours,
			benefits: 			benefits,
			email: 				email,
			password: 			password,
			sign_in_count: 		sign_in_count,
			confirmation_token: confirmation_token,
			confirmed_at: 		confirmed_at,
			failed_attempts: 	failed_attempts,
			picture: 			Faker::Avatar.image 

		)
end

500.times do |n|
	Job.create!(
			position: 			Faker::Company.profession,
			category_id:      	rand(1..25),
			user_id: 			rand(1..10),
			salary: 			rand(5000..20000),
			summary: 			Faker::Lorem.sentences(25)
		)
end
AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')