class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.references :category, index: true, foreign_key: true
      t.string :position
      t.string :summary

      t.timestamps null: false
    end
  end
end
