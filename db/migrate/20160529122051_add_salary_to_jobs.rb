class AddSalaryToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :salary, :decimal, precision: 10, scale: 2
  end
end
