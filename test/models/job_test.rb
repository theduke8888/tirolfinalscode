require 'test_helper'

class JobTest < ActiveSupport::TestCase
	def setup
		@category = categories(:category1)
		@job = @category.jobs.build(position: "Position", summary: "Summary")
	end

	test "should be valid" do
		assert @job.valid?
	end

	test "name should be present" do
		@job.position = nil
		assert_not @job.valid?
	end

	test "summary should be present" do
		@job.summary = nil
		assert_not @job.valid?
	end

end
