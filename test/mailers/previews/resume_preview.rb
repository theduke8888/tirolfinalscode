# Preview all emails at http://localhost:3000/rails/mailers/resume
class ResumePreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/resume/application_sent
  def application_sent
    applicant = Applicant.first
    Resume.application_sent(applicant)
  end

end
