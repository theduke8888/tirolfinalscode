require 'test_helper'

class ResumeTest < ActionMailer::TestCase
  test "application_sent" do
    mail = Resume.application_sent
    assert_equal "Application sent", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
