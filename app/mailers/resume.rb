class Resume < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.resume.application_sent.subject
  #
  def application_sent(applicant)
  	@applicant = applicant
    mail from: 'Pinoy Job Portal <theduke8777@gmail.com>', to: @applicant.email, subject: 'Pinoy Job Portal'	
  end
end
