class ApplicationMailer < ActionMailer::Base
  default from: "theduke8777@gmail.com"
  layout 'mailer'
end
