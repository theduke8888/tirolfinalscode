class Applicant < ActiveRecord::Base
  belongs_to :job
  default_scope -> {order(created_at: :desc )}
  before_save :downcase_email
 
  validates :name, presence: true
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, format:{with: VALID_EMAIL_REGEX}, case_sensitive: false;
  validates :contact, presence: true
  validates :address, presence: true
  validates :resume, presence: true
  mount_uploader :resume, ResumeUploader
  validate :resume_size

  private

  	def resume_size
  		if resume.size > 2.megabytes
  			errors.add(:resume, "Should less than 2MB!")
  		end
  	end

    def downcase_email
      self.email = email.downcase
    end
	 	  
end
