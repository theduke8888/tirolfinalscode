class Job < ActiveRecord::Base
  belongs_to :category
  belongs_to :user
  has_many :applicants, dependent: :destroy
  before_save :capitalize_job
  validates :position, presence: :true
  validates :summary, presence: :true
  default_scope -> {order(updated_at: :desc)}


  def self.search(search)
    where("position ILIKE ? OR summary ILIKE ?", "%#{search}%", "%#{search}%")
  end
 
  private

  	def capitalize_job
  		self.position = position.titleize
  	end
end
