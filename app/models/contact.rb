class Contact < MailForm::Base
  attribute :name,    validate: true
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  attribute :email,   validate: VALID_EMAIL_REGEX
  attribute :message,   validate: true
  attribute :nickname,  captcha: true


  # Declare the e-mail headers. It accepts anything the mail method
  # in ActionMailer accepts.

  def headers
    {
    subject:    "Inquiry",
    to:       "theduke8777@gmail.com",
    from:       %("#{name}" <#{email}>)
    }
  end

end
