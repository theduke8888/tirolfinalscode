class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_many :jobs, dependent: :destroy
 
  validates :name, presence: :true
  validates :company, presence: :true
  validates :address, presence: :true
  validates :overview, presence: :true
  validates :overview, presence: :true
  validates :working_hours, presence: :true
  validates :benefits, presence: :true
  mount_uploader :picture, PictureUploader
  validate :picture_size

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable, :lockable


  private
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "Should less than 5 megabytes!")
      end
    end
end
