class Category < ActiveRecord::Base
	has_many :jobs, dependent: :destroy
	validates :name, presence: true
	default_scope -> {order(name: :asc)}
end
