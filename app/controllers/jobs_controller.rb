class JobsController < ApplicationController
	
	before_action :get_clients
	before_action :signed_user, only:[:new, :create, :edit]
	before_action :correct_user, only:[:update, :edit, :destroy]
	

	def index
		@jobs = Job.paginate(page: params[:page], per_page: 10)
	end

	def new
		@job = current_user.jobs.new
	end

	def create
		@job = current_user.jobs.build(job_params)
		if @job.save 
			redirect_to @job, notice: 'Jobs successfully saved'
			#flash[:success] = "Job successfully saved"
			#redirect_to @job
		else
			flash.now[:danger] = "Error saving, please check the details"
			render 'new'
		end
	end

	def show	
		@job = Job.find(params[:id])
		@applicant = Applicant.new
	end

	def edit
		@job = current_user.jobs.find(params[:id])	
	end

	def update 
		@job = current_user.jobs.find(params[:id])
		if @job.update_attributes(job_params)
			flash[:success] = "Update Saved"
			redirect_to @job
		else
			render 'edit'
		end
	end

	def destroy
		Job.find(params[:id]).destroy
		flash[:success] = "Job Deleted"
		redirect_to current_user
	end

	def show_result
		@results = Job.search(params[:search]).paginate(page: params[:page], per_page: 10)
		
		if @results.blank?
			flash.now[:info] = "No found Keyword"
			@jobs = Job.paginate(page: params[:page], per_page: 10)	
		end
	end 

	private

		def job_params
			params.require(:job).permit(:position, :category_id, :salary, :summary)
		end

		def get_clients
			@clients_logo = User.order('random()').limit(5)
		end

		def  signed_user
			unless user_signed_in? 
				redirect_to jobs_url
			end
		end
		def correct_user
			@job = current_user.jobs.find_by(id: params[:id])
			if @job.nil? 
				flash[:danger] = "You don't have a  previlige"
				redirect_to jobs_url
			end
		end

	
end




