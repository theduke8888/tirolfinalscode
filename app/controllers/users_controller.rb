class UsersController < ApplicationController

	#before_action :signed_user, only:[:show, :show_applicant]
	before_action :correct_user, only:[:show, :show_applicant]
	def show
		@jobs = current_user.jobs.paginate(page: params[:jobs_page], per_page: 10)
		@app_jobs = current_user.jobs.joins(:user, :applicants).group('jobs.id').paginate(page: params[:app_jobs], per_page: 10)

	end

	def show_applicant
		@applicant = Applicant.find(params[:id])
		respond_to do |format|
		 format.js {render 'show_applicant'} 
		end
	end

	def show_employer
		@employer = User.find(params[:id])
	end	

	
	
	private 
		def signed_user
			unless user_signed_in?
				redirect_to jobs_url, alert: "You don't have a previlige"
			end
		end

		def correct_user
			unless current_user.eql? User.find(params[:id])
				redirect_to jobs_url, alert: "You don't have a previlige"
			end
		end
end
