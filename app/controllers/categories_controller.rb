class CategoriesController < ApplicationController

	before_action :redirect_all, except:[:show]

	def index
	#	@categories = Category.paginate(page: params[:page], page: 20)
	#	@categories = Category.all
		@categories = Category.paginate(page: params[:page])
	end

	def new
		@category = Category.new
	end

	def create 
		@category =  Category.new(category_params)
		if @category.save
			flash[:success] = "New Category Saved"
			redirect_to categories_url
		else
			render 'new'
		end
	end

	def show
		@category = Category.find(params[:id])
		@jobs = @category.jobs.paginate(page: params[:page], per_page: 10)
		@clients = User.all
	end

	def edit
		@category = Category.find(params[:id])
	end

	def update 
		@category = Category.find(params[:id])
		if @category.update_attributes(category_params)
			flash[:success] = "Category Name Updated"
			redirect_to categories_url
		else
			render 'edit'
		end

	end

	def destroy
		Category.find(params[:id]).destroy
		flash[:success] = "Category deleted"
		redirect_to categories_url
	end
	private 

		def category_params
			params.require(:category).permit(:name)
		end

		def redirect_all
			redirect_to jobs_url
		end
end
