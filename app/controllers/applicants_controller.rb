class ApplicantsController < ApplicationController

	def index
		redirect_to jobs_url
	end
	def create
		
		@applicant = Applicant.new(applicant_params)
		if @applicant.save
			Resume.application_sent(@applicant).deliver_now
			render 'submitted'
		else
			session[:errors] = @applicant.errors.full_messages
			redirect_to :back
		end
	end
	
	def destroy
		Applicant.find(params[:id]).destroy 
		flash[:success] = "Applicant Deleted"
		redirect_to current_user
	end

	def show
		@applicant = Applicant.find(params[:id])
	end

	
	private 
		def applicant_params
			params.require(:applicant).permit(:name, :email, :contact, :address, :job_id, :resume)
		end

	

end
