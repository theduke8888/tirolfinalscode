class ContactsController < ApplicationController
	def index
		redirect_to jobs_path
	end
	
	def new
		@contact = Contact.new
	end

	def create
		@contact = Contact.new(params_contact)
		@contact.request = request
		unless @contact.deliver
			flash.now['danger'] = 'Cannot send messages'
			render 'new'
		end
	end

	private
		def params_contact
			params.require(:contact).permit(:name, :email, :message, :nickname)
		end

end
