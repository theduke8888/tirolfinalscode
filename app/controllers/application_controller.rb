class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :get_categories
  before_action :get_latest_jobs
  before_action :get_clients_footer
  before_action :set_cache_header

  protected

  	def configure_permitted_parameters
 		devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :company, :address, :overview, :working_hours, :benefits, :picture])
		devise_parameter_sanitizer.permit(:account_update, keys: [:name, :company, :address, :overview, :working_hours, :benefits, :picture])
	end


  private

   def after_sign_in_path_for(resource_or_scope)
   	if resource_or_scope.is_a?(AdminUser)
        admin_dashboard_path
      else
        current_user
    end
   end

   def after_sign_out_path_for(resource_or_scope)
   	  jobs_url
   end

    def get_categories
      @categories_result = Category.all
    end
    
    def get_latest_jobs
      @latest_jobs = Job.all.limit(15)
    end

    def get_clients_footer
      @clients = User.order('random()').limit(10)
    end

    def set_cache_header
      response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
      response.headers["Pragma"] = "no-cache"
      response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
    end

end
