ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    
    #div class: "blank_slate_container", id: "dashboard_default_message" do
    #  span class: "blank_slate" do
    #   span I18n.t("active_admin.dashboard_welcome.welcome")
    #    small I18n.t("active_admin.dashboard_welcome.call_to_action")
    #  end
        panel "Recent Jobs" do
            table_for Job.order(created_at: :desc).limit(5) do 
                column :position
                column :category_id
                column :user_id
                column :salary, sortable: :salary do |base|
                div class: 'salary' do
                    number_to_currency(base.salary, unit: 'PHP')
                end
             end
            end
            strong { link_to "View All Jobs", admin_jobs_path}
        end

        panel "Employers" do
            table_for User.all.limit(5) do
                column :company 
                column :name
                column :address
                column "Registed on" , :created_at
            end
            strong { link_to "View All Employers", admin_users_path }
        end
    #end

    # Here is an example of a simple dashboard with columns and panels.
    #
    # columns do
    #   column do
    #     panel "Recent Posts" do
    #       ul do
    #         Post.recent(5).map do |post|
    #           li link_to(post.title, admin_post_path(post))
    #         end
    #       end
    #     end
    #   end

    #   column do
    #     panel "Info" do
    #       para "Welcome to ActiveAdmin."
    #     end
    #   end
    # end
  end # content
end
