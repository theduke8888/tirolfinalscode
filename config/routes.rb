Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
#	devise_for :users
	devise_for :users, controllers: { registrations: 'users/registrations'}
	#devise_for :users, :controllers => { :registrations => :registrations }
    root                                 'static_pages#index'
    get 'new_cat',		         	             to: 'categories#new'

    get 'contact_us',                        to: 'contacts#new'

    resources :contacts, only: [:new, :create, :index]
    resources :categories
    resources :jobs do 
      collection do
        get 'show_result'
      end
    end
    
    resources :users, only:[:show] do
      collection do 
        get 'show_applicant'
        get 'new_release'
        get 'show_employer'
        get 'show_employer_jobs'
      end
    end

    get '/users/show_applicant/:id'               => 'users#show_applicant', as: :show_applicant
    get '/users/show_employer/:id'                => 'users#show_employer', as: :show_employer
    get '/users/show_employer_jobs/:id'           => 'users#show_employer_jobs', as: :show_employer_jobs
   # get '/users/show_applicant/'        => 'users#show_applicant', as: :show_applicant
    #get '/users/new_release'               => 'users#new_release', as: :new_release

   	resources :applicants, only:[:index, :create, :show, :destroy] do
   			collection do
   			get 'submitted'
   	end
   	end
  
  
end
